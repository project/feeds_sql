<?php

/**
 * Parses data from an SQL database.
 */
class FeedsSQLParser extends FeedsParser {

  /**
   * Implements FeedsParser::parse().
   */
  public function parse(FeedsSource $source, FeedsFetcherResult $fetcher_result) {
    $source_config = $source->getConfigFor($this);
    // Construct the standard form of the parsed feed
    $result = new FeedsParserResult();
    $result->title = '';
    $result->description = '';
    $result->link = '';
    // Iterate through the fetcher results
    foreach ($fetcher_result->getRaw() as $index => $row) {
      $result->items[$index] = (array) $row;
    }
    // Create a result object and return it.
    return $result;
  }

  /**
   * Override parent::getMappingSources().
   */
  public function getMappingSources() {
    return parent::getMappingSources();
  }

  /**
   * Override parent::getSourceElement() to use only lower keys.
   */
  public function getSourceElement(FeedsSource $source, FeedsParserResult $result, $element_key) {
    return parent::getSourceElement($source, $result, drupal_strtolower($element_key));
  }

  /**
   * Define defaults.
   */
  public function sourceDefaults() {
    return array();
  }

  /**
   * Source form.
   */
  public function sourceForm($source_config) {
    $form = array();
    return $form;
  }

  /**
   * Define default configuration.
   */
  public function configDefaults() {
    return array();
  }

  /**
   * Build configuration form.
   */
  public function configForm(&$form_state) {
    $form = array();
    return $form;
  }

}
