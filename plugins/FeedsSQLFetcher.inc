<?php

/**
 * @file
 * Fetches data from an SQL database.
 */

/**
 * Fetches data via pdo connection.
 */
class FeedsSQLFetcher extends FeedsFetcher {

  /**
   * Implements FeedsFetcher::fetch().
   */
  public function fetch(FeedsSource $source) {
    $results = array();
    $source_config = $source->getConfigFor($this);
    // Verify the query is a SELECT statement
    $select = strtoupper(substr($source_config['query'], 0, 6));
    if ($select != 'SELECT') {
      drupal_set_message(t('SQL query has to be of the form "SELECT field1, field2 FROM table WHERE conditions"'), 'error');
    }
    else {
      // Run the query on the selected database
      db_set_active($source_config['database']);
      $result = db_query($source_config['query']);
      foreach ($result as $record) {
        $results[] = $record;
      }
      db_set_active();
    }
    return new FeedsFetcherResult($results);
  }
  
  /**
   * Override parent::configDefaults().
   */
  public function configDefaults() {
    return array(
      'databases' => array('default'),
    );
  }

  /**
   * Override parent::configForm().
   */
  public function configForm(&$form_state) {
    global $databases;
    $form = array();
    $form['databases'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Database'),
      '#description' => t('Select the databases from which to allow fetching the data.'),
      '#options' => array_combine(array_keys($databases), array_keys($databases)),
      '#default_value' => $this->config['databases'],
      '#required' => TRUE,
    );
    return $form;
  }
  
  /**
   * Override parent::sourceDefaults().
   */
  function sourceDefaults() {
    return array(
      'query' => '',
      'database' => 'default',
    );
  }
  
  /**
   * Override parent::sourceForm().
   */
  public function sourceForm($source_config) {
    global $databases;
    $form = array();
    $form['query'] = array(
      '#type' => 'textarea',
      '#title' => t('SQL query'),
      '#description' => t('Enter the SQL query which will fetch the data to be imported.'),
      '#default_value' => isset($source_config['query']) ? $source_config['query'] : '',
    );
    $form['database'] = array(
      '#type' => 'select',
      '#title' => t('Database'),
      '#description' => t('Select the database from which to fetch the data.'),
      '#options' => array_combine(array_keys($databases), array_keys($databases)),
      '#default_value' => isset($source_config['database']) ? $source_config['database'] : 'default',
      '#required' => TRUE,
    );
    return $form;
  }

}
